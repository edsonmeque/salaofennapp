@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card">
        <card class="body">
            <passport-clients></passport-clients>
            <passport-authorized-clients></passport-authorized-clients>
            <passport-personal-access-tokens></passport-personal-access-tokens>
        </card>
    </div>
</div>
@endsection