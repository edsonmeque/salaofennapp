<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'PassportController@login');
    Route::post('register', 'PassportController@register');

Route::group(['middleware' => 'api'], function ($router) {
    

    Route::get('/', function (Request $request) {
        return response()->json(['message' => 'Successfully connected API with app salan Fenn ']);
    });

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    Route::fallback(function () {
        return response()->json(['message' => 'Route not found', 'status' => 'connected']);
    });
    Route::resource('companies', CompanyController::class)->except(['create', 'edit']);
    Route::resource('employee', EmployeeController::class)->except(['create', 'edit']);
    Route::resource('schedules', ScheduleController::class)->except(['create', 'edit']);
    Route::resource('service', ServiceController::class)->except(['create', 'edit']);
});