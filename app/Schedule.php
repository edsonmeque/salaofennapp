<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Employee;
use App\Service;
use App\User;

class Schedule extends Model
{
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employees_id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class, 'services_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }
}
