<?php

namespace App\Http\Controllers;

use App\Schedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Schedule::all();

        if($schedules)

            return response()->json($schedules);

        return response()->json(['error' => 'Response not found']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $schedule = new Schedule();
        $schedule->schedule_date = $request->schedule_date;
        $schedule->employees_id =$request->employees_id;
        $schedule->services_id =$request->services_id;
        $schedule->save();
        if ($schedule)

            return response()->json($schedule);

        return response()->json(['error' => 'Resource  not storege']);

    }

    /**
     * Display the specified resource.
     * 
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $schedule = Schedule::find($id);

        if ($schedule)

            return response()->json($schedule);

        return response()->json(['error' => 'Response  not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $schedule = Schedule::find($id);
        $schedule = new Schedule();
        $schedule->schedule_date = $request->schedule_date;
        $schedule->employees_id = $request->employees_id;
        $schedule->services_id = $request->services_id;
        $schedule->save();
        if ($schedule)

            return response()->json($schedule);

        return response()->json(['error' => 'Resource  not storege']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedule = Schedule::find($id);
        
        if ($schedule) {

            $schedule->delete();

            return response()->json($schedule);
        }

        return response()->json(['error' => 'Resource  not remove']);
    }
}
