<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class PassportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }


    public function register(Request $request)
    {
        $this->validate($request, [
            //'name' => 'required|min:4',
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        $user = User::create([
          //  'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'activated'=>1
        ]);

        $token = $user->createToken($user->email)->accessToken;

        return response()->json(['token' => $token], 200);
    }



    public function login(Request $request)
    {
        $data = [
            'email' => $request->email,
            
            'password' => $request->password
        ];

        if (auth()->attempt($data)) {

            $token = auth()->user()->createToken('LaravelAuthApp')->accessToken;

            return response()->json(['token' => $token], 200);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    } 


}
