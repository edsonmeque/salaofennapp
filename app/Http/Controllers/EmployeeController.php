<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::with('company','user','services.schedules','schedules')->get();

        if ($employees)

            return response()->json($employees);

        return response()->json(['error' => 'Response not found']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee =new Employee();
        $employee->first_name= $request->first_name;
        $employee->last_name= $request->last_name;
        $employee->companies_id = $request->companies_id;
        $employee->users_id = $request->users_id;
        $employee->save();

        if ($employee)

            return response()->json($employee);

        return response()->json(['error' => 'Resource  not storege']);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);

        if ($employee)

            return response()->json($employee);

        return response()->json(['error' => 'Response  not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $employee = Employee::find($id);

        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->companies_id = $request->companies_id;
        $employee->users_id = $request->users_id;
        $employee->save();

        if ($employee)

            return response()->json($employee);

        return response()->json(['error' => 'Resource  not storege']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);

        if ($employee) {

            $employee->delete();

            return response()->json($employee);
        }

        return response()->json(['error' => 'Resource  not remove']);
        
    }
}
