<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // caregando uma lista de empresa(saloes) cadastrada no sisttema 
        $companies = Company::all();

        // verifica se esse array tem empresa cadastrada 
        if ($companies)

            return response()->json($companies);

        return response()->json(['error' => 'Response not found']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = new Company();
        $company->name = $request->name;
        $company->latitude = $request->latitude;
        $company->longitude = $request->longitude;
        $company->phone = $request->phone;
        $company->social_link = $request->social_link;
        $company->save();

        if ($company)

            return response()->json($company);

        return response()->json(['error' => 'Resource  not storege']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);

        if ($company)

            return response()->json($company);

        return response()->json(['error' => 'Response  not found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $company = Company::find($id);
        $company->name = $request->name;
        $company->latitude = $request->latitude;
        $company->longitude = $request->longitude;
        $company->phone = $request->phone;
        $company->social_link = $request->social_link;
        $company->save();

        if ($company)

            return response()->json($company);

        return response()->json(['error' => 'Resource  not update']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
       

        if ($company){

           $company->delete();

        return response()->json($company);
        } 
            
        return response()->json(['error' => 'Resource  not remove']);
    }
}
