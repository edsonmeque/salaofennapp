<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Schedule;
use App\Company;
use App\Service;

class Employee extends Model
{
    public function company()
    {
        return $this->belongsTo(Company::class, 'companies_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class, 'employees_id');
    }

    public function services()
    {
        return $this->hasMany(Service::class, 'employees_id');
    }
}
