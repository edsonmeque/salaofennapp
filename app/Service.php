<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Employee;
use App\Schedule;
class Service extends Model
{
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employees_id');
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class, 'services_id');
    }
}
